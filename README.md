# Same Hash Code - Générer trois chaînes différentes avec le même hachage

Ce projet console utilise .NET Core 6 pour générer trois chaînes différentes avec le même hachage en utilisant une fonction `ProduceThreeDifferentString()`.

## Prérequis

- [.NET Core 6 SDK](https://dotnet.microsoft.com/download/dotnet/6.0)
- Visual Studio / Rider / VS Code

## Compilation et exécution

1. Clonez le dépôt ou téléchargez les fichiers sources du projet.

2. Ouvrez une fenêtre de terminal et accédez au répertoire racine du **projet** (_qui contient le fichier .csproj_).

3. Exécutez la commande suivante pour compiler le projet :

   ```shell
   dotnet build

4. Une fois la compilation réussie, exécutez la commande suivante pour exécuter l'application :

   ```shell
   dotnet run
   
5. L'application affichera les trois chaînes générées avec leur hachage respectif :

   `A : 2326817, B : 2791856, C : 5177984

   A Hash Code : 994744149, B Hash Code : 994744149, C Hash Code : 994744149`

