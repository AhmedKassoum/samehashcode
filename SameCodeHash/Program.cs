﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

class Program
{
    static void Main()
    {
        var stopwatch = Stopwatch.StartNew();

        var result = ProduceStringsWithSameHashCode();

        stopwatch.Stop();

        Console.WriteLine($"A: {result.Item1}, B: {result.Item2}, C: {result.Item3}");
        Console.WriteLine($"A Hash Code: {result.Item1.GetHashCode()}, B Hash Code: {result.Item2.GetHashCode()}, C Hash Code: {result.Item3.GetHashCode()}");
        Console.WriteLine($"Execution Time: {stopwatch.ElapsedMilliseconds} ms");
    }

    static (string, string, string) ProduceStringsWithSameHashCode()
    {
        var stringDictionary = new ConcurrentDictionary<int, List<string>>();
        var blockingCollection = new BlockingCollection<Task>();

        var random = new Random();
        const int LENGTH = 9;


        Task producerTask = Task.Run(() =>
        {
            for (int counter = 1; counter <= int.MaxValue; counter++)
            {
                //string myString = GetStringFromCounter(counter);
                string myString = GenerateRandomString(random, LENGTH);
                int hashCode = myString.GetHashCode();

                if (stringDictionary.TryGetValue(hashCode, out var stringList))
                {
                    stringList.Add(myString);

                    if (stringList.Count == 3)
                        return;
                }
                else
                {
                    stringList = new List<string> { myString };
                    if (stringDictionary.TryAdd(hashCode, stringList))
                    {
                        if (stringList.Count == 3)
                            return;
                    }
                }
            }
        });

        List<Task> consumerTasks = new List<Task>();
        for (int i = 0; i < Environment.ProcessorCount; i++)
        {
            consumerTasks.Add(Task.Run(() =>
            {
                foreach (var task in blockingCollection.GetConsumingEnumerable())
                {
                    task.Wait();
                }
            }));
        }

        Task.WhenAll(producerTask).Wait();
        blockingCollection.CompleteAdding();
        Task.WhenAll(consumerTasks).Wait();

        var finalList = stringDictionary.Values.First(list => list.Count == 3);
        return (finalList[0], finalList[1], finalList[2]);
    }

    static string GetStringFromCounter(int counter)
    {
        return counter.ToString();
    }

    static string GenerateRandomString(Random random, int length)
    {
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
    }
}
